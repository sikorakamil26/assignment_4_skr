package comparators;
import data.Person;

import java.util.Comparator;

public final class AllComparator implements Comparator<Person> {

    @Override
    public int compare(Person person1, Person person2) {
        int surnameCompare = person1.getSurname().compareTo(person2.getSurname());
        if(surnameCompare != 0) return surnameCompare;

        int firstNameCompare = person1.getFirstName().compareTo(person2.getFirstName());
        if(firstNameCompare != 0) return firstNameCompare;

        int birthDateCompare = person1.getBirthdate().compareTo(person2.getBirthdate());
        return birthDateCompare;
    }
}