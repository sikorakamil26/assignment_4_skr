package tests;

import comparators.AllComparator;
import comparators.BirthdateComparator;
import comparators.FirstNameComparator;
import data.InputParser;
import data.Person;
import data.PersonDatabase;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class AppTest {
    InputParser inputParser;
    PersonDatabase personDatabase;
    private static final DateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    @Before
    public void setUp(){
        inputParser = new InputParser();
        personDatabase = new PersonDatabase();

    }
    @Test
    public void shouldParseFile(){
        final List<Person> parse = inputParser.parse();
        System.out.println(parse);
    }

    @Test
    public void shouldReturnSortedByFirstName(){
        List<Person> result = personDatabase.sortedByFirstName();
        Assert.assertTrue(isSorted(result, new FirstNameComparator()));
    }

    @Test
    public void shouldReturnSortedByAll(){
        List<Person> result = personDatabase.sortedBySurnameFirstNameAndBirthdate();
        Assert.assertTrue(isSorted(result, new AllComparator()));
    }

    @Test
    public void shouldReturnSortedByBirthDate(){
        List<Person> result = personDatabase.sortedByBirthdate();
        Assert.assertTrue(isSorted(result, new BirthdateComparator()));
    }

    @Test
    public void shouldReturnBornAt(){
        try {
            List<Person> result = personDatabase.bornOnDay(FORMAT.parse("2007-05-19"));
            System.out.println(result);
            Assert.assertTrue(
                    result.stream().allMatch(person -> {
                        try {
                            return person.getBirthdate().equals(FORMAT.parse("2007-05-19")) ? true:false;
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        return false;
                    })
            );
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public static boolean isSorted(List<Person> people, Comparator<Person> comparator){

        if (people.isEmpty() || people.size() == 1) {
            return true;
        }
        Iterator<Person> iter = people.iterator();
        Person current, previous = iter.next();
        while (iter.hasNext()) {
            current = iter.next();
            if (comparator.compare(previous, current) > 0) {
                return false;
            }
            previous = current;
        }
        return true;
    }


}
