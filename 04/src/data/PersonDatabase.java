package data;

import comparators.AllComparator;
import comparators.BirthdateComparator;
import comparators.FirstNameComparator;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public final class PersonDatabase {

    List<Person> people;
    InputParser input;

    public PersonDatabase(){
         input = new InputParser();
         people = input.parse();
    }

    public List<Person> sortedByFirstName() {
        List<Person> result = people;
        result.sort(new FirstNameComparator());
        return result;
    }

    public List<Person> sortedBySurnameFirstNameAndBirthdate() {
        List<Person> result = people;
        result.sort(new AllComparator());
        return result;
    }

    public List<Person> sortedByBirthdate() {
        List<Person> result = people;
        result.sort(new BirthdateComparator());
        return result;
    }

    public List<Person> bornOnDay(Date date) {
        List<Person> result = people;
        return result.stream().filter(person -> person.getBirthdate().equals(date)).collect(Collectors.toList());
    }
}