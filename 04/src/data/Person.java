package data;

import java.util.Date;

public class Person implements Comparable<Person> {

    private final String firstName;
    private final String surname;
    private final Date birthdate;

    public Person(String firstName, String surname, Date birthdate) {
        this.firstName = firstName;
        this.surname = surname;
        this.birthdate = birthdate;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSurname() {
        return surname;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", surname='" + surname + '\'' +
                ", birthdate=" + birthdate +
                '}' + "\n";
    }

    @Override
    public int compareTo(Person otherPerson) {
        int surnameCompare = surname.compareTo(otherPerson.surname);
        if(surnameCompare != 0) return surnameCompare;

        int firstNameCompare = firstName.compareTo(otherPerson.firstName);
        if(firstNameCompare != 0) return firstNameCompare;

        int birthDateCompare = birthdate.compareTo(otherPerson.birthdate);
        return birthDateCompare;
    }
}