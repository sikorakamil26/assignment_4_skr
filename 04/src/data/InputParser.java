package data;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class InputParser {

    private static final DateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private static final String FILE_NAME = "/Users/kamilsikora/Documents/Studia/UTP/Assignments/04/src/tests/people.txt";

    private static final String FIRST_NAME_GROUP = "firstName";
    private static final String SURNAME_GROUP = "surname";
    private static final String BIRTHDATE_GROUP = "birthdate";

    private static final String NAME_PATTERN = "(?:[A-ZĄŻŹĆŚĘĆŃ]([a-łżźćńąęóń])+)";
    private static final String FIRST_NAME_PATTERN= "(?<" + FIRST_NAME_GROUP + ">" + NAME_PATTERN + ")";
    private static final String SURNAME_PATTERN = "(?<" + SURNAME_GROUP + ">" + NAME_PATTERN + ")";

    private static final String YEAR_PATTERN = "(?:[0-9]{4})"; //4 cyfry od 0-9
    private static final String MONTH_PATTERN = "(?:(?:[0][1-9])|(?:[1][0-2]))";
    private static final String DAY_PATTERN = "(?:(?:[0][1-9])|(?:[1-2][0-9])|(?:[3][0-1]))";
    private static final String SEPARATOR = "-";

    private static final String BIRTHDATE_PATTERN = "(?<" + BIRTHDATE_GROUP + ">" + YEAR_PATTERN + SEPARATOR + MONTH_PATTERN + SEPARATOR + DAY_PATTERN + ")";

    private static final String WHITESPACE_PATTERN = "(?:[ \t]+)";

    private static final String LINE_PATTERN = FIRST_NAME_PATTERN + WHITESPACE_PATTERN + SURNAME_PATTERN + WHITESPACE_PATTERN + BIRTHDATE_PATTERN;

    private static final Pattern LINE_REGEX = Pattern.compile(LINE_PATTERN);


    public static List<Person> parse() {

        List<Person> result = new ArrayList<>();
        try(
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        new FileInputStream(FILE_NAME), StandardCharsets.UTF_8));
        ){
            String nextLine;
            int line = 0;
            while((nextLine = br.readLine()) != null){
                line++;
                Person person = parsePerson(nextLine, line);
                if(person != null) result.add(person);
            }
            return result;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    public static Person parsePerson(String data, int line){
        Matcher match = LINE_REGEX.matcher(data);
        if(!match.matches()) {
            System.out.println("Sory, wrong data format at line: " + line);
            return null;
        }

        String[] personData = data.split(" ");
        try {
            Person person = new Person(personData[0], personData[1], FORMAT.parse(personData[2]));
            return person;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}